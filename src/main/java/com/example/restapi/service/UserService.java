package com.example.restapi.service;

import com.example.restapi.dto.UserDto;

import javax.xml.bind.ValidationException;
import java.util.List;

public interface UserService {
    UserDto saveUser(UserDto userDto) throws ValidationException;

    void deleteUSer(Integer userId);

    UserDto findByLogin(String login);

    List<UserDto> findAll();


}
