package com.example.restapi.service;

import com.example.restapi.dto.UserDto;
import com.example.restapi.entety.User;
import com.example.restapi.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.xml.bind.ValidationException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
@Service
@AllArgsConstructor
public class DefaultUsersService implements UserService {

    private final UserRepository userRepository;
    private final UsersConverter usersConverter;


    @Override
    public UserDto saveUser(UserDto userDto) throws ValidationException {
        validateUserDto(userDto);
        User savedUser = userRepository.save(usersConverter.fromUserDtoToUser(userDto));
        return usersConverter.fromUserToUserDto(savedUser);
    }

    @Override
    public void deleteUSer(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public UserDto findByLogin(String login) {
       User user =  userRepository.findByLogin(login);
        if (user != null) {
            return usersConverter.fromUserToUserDto(user);
        }
        return null;
    }

    @Override
    public List<UserDto> findAll() {
        List<User> list = userRepository.findAll();
        return list.stream().map(usersConverter::fromUserToUserDto).collect(Collectors.toList());
    }

    public void validateUserDto(UserDto userDto) throws ValidationException {
        if (isNull(userDto)) {
            throw new ValidationException("Object is null");
        }
        if (isNull(userDto.getLogin()) || userDto.getLogin().isEmpty()) {
            throw new ValidationException("Login is Empty");
        }
    }

}
