package com.example.restapi.service;

import com.example.restapi.dto.UserDto;
import com.example.restapi.entety.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.xml.bind.ValidationException;

import static java.util.Objects.isNull;
@Component

public class UsersConverter {

    public User fromUserDtoToUser(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setLogin(userDto.getLogin());
        user.setEmail(userDto.getEmail());
        return user;
    }

    public UserDto fromUserToUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .login(user.getLogin())
                .email(user.getEmail())
                .build();
    }
}
