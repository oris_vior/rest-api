package com.example.restapi.repository;

import com.example.restapi.entety.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer> {
    User findByLogin(String login);

}
